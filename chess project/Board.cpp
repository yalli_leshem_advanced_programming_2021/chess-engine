#include "Board.h"

Board::Board() : _positions{}, _whitesTurn(true) {
}

Board::~Board()
{
	for (int y = 0; y < 8; y++)
	{
		for (int x = 0; x < 8; x++)
		{
			delete this->_positions[y][x];
			this->_positions[y][x] = nullptr;
		}
	}
}

bool Board::isWhitesTurn() const
{
	bool whitesTurn;
	whitesTurn = this->_whitesTurn;
	return whitesTurn;
}

void Board::nextTurn()
{
	this->_whitesTurn = this->_whitesTurn ? false : true;
}

Piece* Board::getPiece(const Pos pos)
{
	Piece* pPiece = this->_positions[pos.getY()][pos.getX()];
	
	return pPiece;
}

void Board::swapPieces(const Pos p1, const Pos p2)
{
	std::swap(this->_positions[p2.getY()][p2.getX()], this->_positions[p1.getY()][p1.getX()]);
}

char Board::playTurn(std::string msg)
{
	std::string p1 = msg.substr(0,2);
	std::string p2 = msg.substr(2,2);

	Pos currPos = Pos(p1);
	Pos newPos = Pos(p2);
	
	Piece* piece = this->getPiece(currPos);
	Codes moveCode = piece->movePiece(newPos, *this);

	switch (moveCode)
	{
	case Codes::VALID:
		return '0';
	case Codes::CHECK:
		return '1';
	case Codes::NO_PIECE:
		return '2';
	case Codes::SAME_COLOR:
		return '3';
	case Codes::SELF_CHECK:
		return '4';
	case Codes::ILLEGAL_POS:
		return '5';
	case Codes::ILLEGAL_MOVE:
		return '6';
	case Codes::SAME_DST_AND_SRC_POS:
		return '7';
	case Codes::CHECKMATE:
		return '8';

	default:
		return 'a';
		break;
	}
}

void Board::insertPiece(Piece* piece)
{
	Pos pos = piece->getPos();
	this->_positions[pos.getY()][pos.getX()] = piece;
}
