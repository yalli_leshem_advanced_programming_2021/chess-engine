#include "Pawn.h"

Pawn::Pawn(const bool white, const Pos pos) : Piece(white, pos, false)
{}

std::vector<struct positionAndCode> Pawn::getAllMoves(Board& board) const
{
	std::vector<struct positionAndCode> allMoves;

	std::vector<Pos> possibleEatingPos = checkIfCanEat();
	std::vector<Pos> possibleMoves = checkIfCanMoveUp();

	// add a code to the eating moves
	for (Pos ePos : possibleEatingPos)
	{
		// make new position type
		struct positionAndCode posNCode;
		posNCode.pos = ePos;
		posNCode.code = Codes::ILLEGAL_MOVE; // default

		//	only if the piece isnt the same color of the Pawn the move is valid
		Piece* piece = board.getPiece(ePos);
		if (!piece->isEmpty() && piece->isWhite() != this->_white)
		{
			posNCode.code = Codes::VALID;
		}

		allMoves.push_back(posNCode);
	}

	bool isBlocked = false;
	for (Pos mPos : possibleMoves)
	{
		// make new position type
		struct positionAndCode posNCode;
		posNCode.pos = mPos;
		posNCode.code = Codes::ILLEGAL_MOVE; // default is that its blocked by another piece

		//	only if the piece is empty and we didnt check another move before the move is valid
		Piece* piece = board.getPiece(mPos);
		if (piece->isEmpty())
		{
			posNCode.code = (!isBlocked) ? Codes::VALID : Codes::ILLEGAL_MOVE;
		}
		else if (piece->isWhite() == this->_white && !isBlocked) // if the pieces are the same color, cange code
		{
			posNCode.code = Codes::SAME_COLOR;
		}

		allMoves.push_back(posNCode);
	}

	return allMoves;
}

std::vector<Pos> Pawn::checkIfCanMoveUp() const
{
	const int MIN_Y = 0;
	const int MAX_Y = 7;
	const int FIRST_Y_OF_WHITE = 1;
	const int FIRST_Y_OF_BLACK = 6;

	std::vector<Pos> possibleMoves;

	// if white, can only go up, if black only down, add this to current y to get the naxt one
	int nextY = (this->_white) ? 1 : -1;
	int currY = this->_pos.getY();

	// make sure the piece didnt reach the end
	if (MIN_Y < (currY + nextY) && (currY + nextY) < MAX_Y)
	{
		 possibleMoves.push_back(Pos(this->_pos.getX(), (currY + nextY)));

		// if the pawn is still at its first position, add the 2nd one 
		if (this->_white && FIRST_Y_OF_WHITE == this->_pos.getY() || !this->_white && FIRST_Y_OF_BLACK == this->_pos.getY())
		{
			// y + nextY + nextY == y + 2 || y - 2 
			possibleMoves.push_back(Pos(this->_pos.getX(), (currY + nextY + nextY)));
		}
	}
	
	return possibleMoves;
}

std::vector<Pos> Pawn::checkIfCanEat() const
{
	const int MIN_X = 0;
	const int MAX_X = 7;
	const int MIN_Y = 0;
	const int MAX_Y = 7;

	std::vector<Pos> possibleEatingPos;

	int currX = this->_pos.getX();
	int currY = this->_pos.getY();
	int y;

	// if white, can only go up
	if (this->_white)
	{
		y = currY + 1;
	}
	else // if black, can only move down
	{
		y = currY - 1;
	}

	// make sure the piece didnt reach the end
	if (MIN_Y <= y && y <= MAX_Y)
	{
		if (MIN_X < currX) // make sure the piece isnt in the corner
		{
			possibleEatingPos.push_back(Pos(currX - 1, y)); // left position
		}
		if (currX < MAX_X) // make sure the piece isnt in the corner
		{
			possibleEatingPos.push_back(Pos(currX + 1, y)); // right position
		}
	}
	return possibleEatingPos;
}
