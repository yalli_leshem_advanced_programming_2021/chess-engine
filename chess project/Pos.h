#pragma once

#include <string>
#include <stdexcept>
#include <optional>
#include <ostream>

/// <summary>
/// A position on a chess board
/// 
/// Goes from a1 = (0, 0) to h8 = (7, 7)
/// </summary>
class Pos {
private:
	short _x;
	short _y;

public:
	static const short MAX = 7;

	/// <summary>
	///	Default constructor: Creates a (0, 0) position
	/// </summary>
	Pos();
	/// <summary>
	/// Create a point by x and y
	/// </summary>
	/// <param name="x">The column value (must be 0-7)</param>
	/// <param name="y">The row value (must be 0-7)</param>
	Pos(short x, short y) throw(std::out_of_range);
	/// <summary>
	/// Create a point from a string in a format like: a1, e5, or b3
	/// 
	/// String must be 2-char long, the first character must be a-h 
	/// and the second must be 1-8
	/// </summary>
	/// <param name="boardNotation">A string in the notation as described</param>
	Pos(const std::string& boardNotation) throw(std::invalid_argument);

	//	Copy constructor
	Pos(const Pos& other);

	//	---------------------------
	//		Getters And Setters
	//	---------------------------

	short getX() const;
	short getY() const;

	void setX(short x) throw(std::out_of_range);
	void setY(short y) throw(std::out_of_range);

	/// <summary>
	/// Returns a string in the notation as described in `Pos(const string&)`
	/// </summary>
	/// <returns></returns>
	std::string getNotation() const;

	//	---------------------------
	//			Utility
	//	---------------------------

	/// <summary>
	/// Returns a new position with the x and y flipped
	/// 
	/// For example: 
	/// c2.flip()  -->  f7
	/// a6.flip()  -->  h3
	/// </summary>
	/// <returns>The flipped position</returns>
	Pos flip() const;

	/// <summary>
	/// Try create a position
	/// </summary>
	/// <param name="x">The x value</param>
	/// <param name="y">The y value</param>
	/// <returns>Some position, or nothing if the position could not be created</returns>
	static std::optional<Pos> tryPosition(short x, short y);

	// https://en.cppreference.com/w/cpp/utility/optional
	/// <summary>
	/// Tries to move the point by a certain amount in the x and y
	/// </summary>
	/// <param name="xMove">Amount to move in the x (row)</param>
	/// <param name="yMove">Amount to move in the y (col)</param>
	/// <returns>
	/// If the position is moved to a valid position, returns it, else 
	/// returns nothing.
	/// <para>
	/// You can use std::optional::has_value to check if a position was 
	/// returned and std::optional::value to use the value inside.
	/// </para>
	/// </returns>
	std::optional<Pos> move(short xMove, short yMove) const;

	/// <summary>
	/// operator == for pos (maybe we'll use it, maybe not.. doesnt hurt to have it around)
	/// </summary>
	/// <param name="other">the position to compare with this one</param>
	/// <returns>true if x == x2 and y == y2</returns>
	bool operator==(const Pos& other) const;
};

std::ostream& operator<<(std::ostream& o, Pos p);
