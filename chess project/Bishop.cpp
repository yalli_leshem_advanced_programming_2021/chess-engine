#include "Bishop.h"

Bishop::Bishop(const bool white, const Pos pos) : Piece(white, pos, false)
{}

std::vector<struct positionAndCode> Bishop::getAllMoves(Board& board) const
{
	return getAllDiagonalMoves(board);
}

std::vector<struct positionAndCode> Bishop::checkOnePartOfPositions(std::vector<Pos> partOfAllPos, Board& board) const
{
	bool isBlocked = false;
	std::vector<struct positionAndCode> allMoves;

	for (Pos pos : partOfAllPos)
	{
		// make new position type
		struct positionAndCode posNCode;
		posNCode.pos = pos;
		posNCode.code = Codes::VALID;

		if (!isBlocked) // if position is already blocked, no need to check the code, it's invalid automatically
		{
			//	If there is a piece, and it is the same color as this
			Piece* piece = board.getPiece(pos);
			if (!piece->isEmpty()) // if theres a piece in this pos
			{
				if (piece->isWhite() == this->_white) {
					posNCode.code = Codes::SAME_COLOR;
					isBlocked = true;
				}
				else // if theres a piece but its a different color, code is valid and the rest of the positions are blocked
				{
					// TODO: check for check/mate add code to check/checkmate accordingly
					posNCode.code = Codes::VALID;
					isBlocked = true;
				}
			}
		}
		else // if blocked then the move is illegal
			posNCode.code = Codes::ILLEGAL_MOVE;

		allMoves.push_back(posNCode);
	}

	return allMoves;
}

std::vector<struct positionAndCode> Bishop::getAllDiagonalMoves(Board& board) const
{
	enum { BELOW_LEFT, BELOW_RIGHT, ABOVE_LEFT, ABOVE_RIGHT };
	std::vector<struct positionAndCode> allMoves;

	// go through all diagonals
	for (int i = BELOW_LEFT; i <= ABOVE_RIGHT; i++)
	{
		// get each part of the positions 
		std::vector<Pos> oneDiagonal = getAllPosDiagonaly(i);

		// now, if the vector is of the positions below the piece, we'll need to reverse them so we'll be able to correctly check them
		if (i == BELOW_LEFT || i == BELOW_RIGHT)
			std::reverse(oneDiagonal.begin(), oneDiagonal.end());

		// get code for each part
		std::vector<struct positionAndCode> parOfAllMoves = checkOnePartOfPositions(oneDiagonal, board);

		// push all the positions to the vector
		for (struct positionAndCode posNcode : parOfAllMoves)
			allMoves.push_back(posNcode);
	}

	return allMoves;
}

std::vector<Pos> Bishop::getAllPosDiagonaly(int returnPart) const
{
	enum { BELOW_LEFT, BELOW_RIGHT, ABOVE_LEFT, ABOVE_RIGHT };

	const int MIN_X = 0;
	const int MAX_X = 8;

	const int MIN_Y = 0;
	const int MAX_Y = 8;

	int currY = this->_pos.getY();
	int currX = this->_pos.getX();

	std::vector<Pos> posBelowLPiece;
	std::vector<Pos> posBelowRPiece;
	std::vector<Pos> posAboveLPiece;
	std::vector<Pos> posAboveRPiece;

	for (int y = MIN_Y; y < MAX_Y; y++)
	{
		int yDifference = currY - y; // to get the next x in the diagonal we simply get the difference between the currenty and the new y and add/sub it from x	

		int xM = currX - yDifference; // xM(minus) 
		int xP = currX + yDifference; // xP(plus)

		if (xM >= MIN_X && xM < MAX_X) // only if x >= 0 and x < 8 the position is valid, and we can make a new position 
		{
			Pos posM = Pos(xM, y); // posM(minus)

			if (y > currY) // if we are checking the upper diagonal: -- == + and because of that we add this position to the posAboveR vector
				posAboveRPiece.push_back(posM);
			else if (currY > y) // if we are checking the lower diagonal: -+ == - and because of that we add this position to the posBelowL vector
				posBelowLPiece.push_back(posM);
		}

		if (xP >= MIN_X && xP < MAX_X) // only if x >= 0 and x < 8 the position is valid, and we can make a new position 
		{
			Pos posP = Pos(xP, y); // posP(plus)

			if (y > currY) // if we are checking the upper diagonal: -+ == - and because of that we add this position to the posAboveL vector
				posAboveLPiece.push_back(posP);
			else if (currY > y) // if we are checking the lower diagonal: ++ == + and because of that we add this position to the posBelowR vector
				posBelowRPiece.push_back(posP);
		}
	}

	if (BELOW_LEFT == returnPart) // if returnPart is 0 : return the posBelowL vector
		return posBelowLPiece;
	else if (BELOW_RIGHT == returnPart) // if returnPart is 1 : return the posBelowR vector
		return posBelowRPiece;
	else if (ABOVE_LEFT == returnPart) // if returnPart is 2 : return the posAboveL vector
		return posAboveLPiece;
	else // if non of the above was entered : return the posAboveR vector
		return posAboveRPiece;
}