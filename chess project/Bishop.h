#pragma once

#include "Piece.h"
#include "Pos.h"
#include "Board.h"
class Board;

class Bishop : public Piece {
public:
	/// <summary>
	///	Create a bishop piece
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	Bishop(const bool white, const Pos pos);

	std::vector<struct positionAndCode> getAllMoves(Board& board) const override;

private:
	/// <summary>
	/// this function gets a part of all the moves a piece can make (one part of all the diagonals a bishop can make, one part of all the crosses the rook can make etc)
	/// the function returns the positions with there code (code examples can be found in the enum codes) 
	///  the codes this function covers are: 0, 3, 4, 6
	/// (this function is a part of the getAllMoves that every class uses, so I wrote it to prevent code duplicates)
	/// </summary>
	/// <param name="partOfAllPos">each class that needs this function divide all the moves to parts, check the code of the positions in one part</param>
	/// <param name="board">the board itself</param>
	/// <returns>a vector of positions and there code</returns>
	std::vector<struct positionAndCode> checkOnePartOfPositions(const std::vector<Pos> partOfAllPos, Board& board) const;

	/// <summary>
	/// function is used by Bishop and Queen.
	/// </summary>
	/// <param name="board">the board itself</param>
	/// <returns>returns all the positions the piece can go to diagonally + there code</returns>
	std::vector<struct positionAndCode> getAllDiagonalMoves(Board& board) const;

	/// <summary>
	/// function will return all the positions that the piece can go to (diagonally) 
	/// *CAN ONLY RETURN ONE PART AT A TIME (below-left/below-right/above-left/above-right)
	/// for example: if the piece is in [3][3]
	///  the diagonal of positions that are below the piece and before it (left side of the board) are: [2][2], [1][1], [0][0]
	///  the diagonal of positions that are below the piece and after it (right side of the board) are: [2][4], [1][5], [0][6]
	///  the diagonal of positions that are above the piece and before it (left side of the board) are: [4][2], [5][1], [6][0]
	///  the diagonal of positions that are above the piece and after it (right side of the board) are: [4][4], [5][5], [6][6], [7][7]
	/// </summary>
	/// <param name="returnPart">what part will the function return</param>
	/// <returns>
	/// if 0 : returns the diagonal of positions that are below the piece and before it (left side of the board)
	/// if 1 : returns the diagonal of positions that are below the piece and after it (right side of the board)
	/// if 2 : returns the diagonal of positions that are above the piece and before it (left side of the board)
	/// else : returns the diagonal of positions that are above the piece and after it (right side of the board)
	/// /returns>
	std::vector<Pos> getAllPosDiagonaly(int returnPart) const;
};