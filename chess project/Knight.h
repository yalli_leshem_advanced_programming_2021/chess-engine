#pragma once

#include "Piece.h"
#include "Pos.h"
#include "Board.h"
class Board;

class Knight : public Piece {
public:
	/// <summary>
	///	Create a knight piece
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	Knight(const bool white, const Pos pos);

	std::vector<struct positionAndCode> getAllMoves(Board& board) const override;

private:
	/// <summary>
	/// get all the moves a knight can make
	/// </summary>
	/// <returns>returns all the positions the knight can go to (valid and invalid)</returns>
	std::vector<Pos> getAllPos() const;
};