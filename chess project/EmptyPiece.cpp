#include "EmptyPiece.h"

EmptyPiece::EmptyPiece(const Pos pos) : Piece(false, pos, true)
{}

std::vector<struct positionAndCode> EmptyPiece::getAllMoves(Board & board) const
{
	return std::vector<struct positionAndCode>();
}
