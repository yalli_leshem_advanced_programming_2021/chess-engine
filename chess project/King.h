#pragma once

#include "Piece.h"
#include "Pos.h"
#include "Board.h"
class Board;

class King : public Piece {
public:
	/// <summary>
	///	Create a king piece
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	King(const bool white, const Pos pos);

	std::vector<struct positionAndCode> getAllMoves(Board& board) const override;
	bool isKing() const override;

protected:
	/// <summary>
	/// checks if there is any piece that can eat the king if he moves to the new position 
	/// </summary>
	/// <param name="newPos">a position the king wants to go to</param>
	/// <param name="board">the board itself</param>
	/// <returns>true, if ant piece can eat the king in this position</returns>
	bool willBeChecked(Pos newPos, Board& board) const;
};