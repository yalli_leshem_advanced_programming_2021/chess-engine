#pragma once

#include "Piece.h"
#include <vector>
#include "Pos.h"
#include <utility> 

class Piece;

/// <summary>
/// The chess board
/// 
/// contains the board 
/// </summary>
class Board {
private:
	static const int SIZE = 8;
	Piece* _positions[SIZE][SIZE];
	bool _whitesTurn;

public:

	/// <summary>
	///	Default constructor: Creates the board (standard chess board), sets whitesTurn to true
	/// </summary>
	Board();
	
	~Board();

	bool isWhitesTurn() const;
	
	/// <summary>
	/// changes the turn (changes _whitesTurn)
	/// </summary>
	void nextTurn();

	/// <summary>
	/// returns the piece in the wanted position 
	/// </summary>
	/// <param name="pos">the position of the piece</param>
	/// <returns>the piece in pos</returns>
	Piece* getPiece(const Pos pos);

	/// <summary>
	/// switches between two pieces (doesnt check if it's valid)
	/// </summary>
	/// <param name="p1">position of the first piece to swap</param>
	/// <param name="p2">position of the 2nd piece to swap</param>
	void swapPieces(const Pos p1, const Pos p2);

	/// <summary>
	/// function gets the message from main and returns the code according to the move  
	/// </summary>
	/// <param name="msg">the message from the frontend</param>
	/// <returns>the code of the move</returns>
	char playTurn(const std::string msg);

	/// <summary>
	/// inserts a piece into the array
	/// </summary>
	/// <param name="piece">the pointer to the piece</param>
	void insertPiece(Piece* piece);
};
