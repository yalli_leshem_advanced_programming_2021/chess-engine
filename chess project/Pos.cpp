#include "Pos.h"

//	returns true iff 0 <= x <= MAX
bool inRange(short x) {
	return 0 <= x && x <= Pos::MAX;
}

Pos::Pos() : _x(0), _y(0) { }

Pos::Pos(short x, short y) throw(std::out_of_range) {
	if (!inRange(x) || !inRange(y)) {
		throw std::out_of_range("Position out of range");
	}

	this->_x = x;
	this->_y = y;
}

Pos::Pos(const std::string& boardNotation) throw(std::invalid_argument) {
	constexpr char MIN1 = 'a';
	constexpr char MAX1 = 'h';
	constexpr char MIN2 = '1';
	constexpr char MAX2 = '8';

	if (!(boardNotation.length() == 2 &&
		MIN1 <= boardNotation[0] && boardNotation[0] <= MAX1 &&
		MIN2 <= boardNotation[1] && boardNotation[1] <= MAX2)) {

		throw std::invalid_argument("Position notation string not in the correct format");
	}

	this->_x = boardNotation[0] - MIN1;
	this->_y = boardNotation[1] - MIN2;
}

Pos::Pos(const Pos& other) : _x(other._x), _y(other._y) { }

short Pos::getX() const
{
	return this->_x;
}

short Pos::getY() const
{
	return this->_y;
}

void Pos::setX(short x) throw(std::out_of_range)
{
	if (!inRange(x)) {
		throw std::out_of_range("Position x is out of range");
	}

	this->_x = x;
}

void Pos::setY(short y) throw(std::out_of_range)
{
	if (!inRange(y)) {
		throw std::out_of_range("Position x is out of range");
	}

	this->_x = y;
}

std::string Pos::getNotation() const
{
	constexpr char START1 = 'a';
	constexpr char START2 = '1';

	return "" + (START1 + this->_x) + (START2 + this->_y);
}

Pos Pos::flip() const
{
	return Pos(MAX - this->_x, MAX - this->_y);
}

std::optional<Pos> Pos::tryPosition(short x, short y)
{
	return inRange(x) && inRange(y) ?
		std::make_optional(Pos(x, y))
		: std::optional<Pos>();
}

std::optional<Pos> Pos::move(short xMove, short yMove) const
{
	return tryPosition(this->_x + xMove, this->_y + yMove);
}

bool Pos::operator==(const Pos& other) const
{
	return (this->_x == other._x && this->_y == other._y);
}

std::ostream& operator<<(std::ostream& o, Pos p)
{
	return o << p.getNotation();
}
