#pragma once

#include "Piece.h"
#include "Pos.h"
#include "Board.h"
class Board;

class Queen : public Piece {
public:
	/// <summary>
	///	Create a queen piece
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	Queen(const bool white, const Pos pos);

	std::vector<struct positionAndCode> getAllMoves(Board& board) const override;

private:
	/// <summary>
	/// this function gets a part of all the moves a piece can make (one part of all the diagonals a bishop can make, one part of all the crosses the rook can make etc)
	/// the function returns the positions with there code (code examples can be found in the enum codes) 
	///  the codes this function covers are: 0, 3, 4, 6
	/// (this function is a part of the getAllMoves that every class uses, so I wrote it to prevent code duplicates)
	/// </summary>
	/// <param name="partOfAllPos">each class that needs this function divide all the moves to parts, check the code of the positions in one part</param>
	/// <param name="board">the board itself</param>
	/// <returns>a vector of positions and there code</returns>
	std::vector<struct positionAndCode> checkOnePartOfPositions(const std::vector<Pos> partOfAllPos, Board& board) const;

	/// <summary>
	/// function is used by Bishop and Queen.
	/// </summary>
	/// <param name="board">the board itself</param>
	/// <returns>returns all the positions the piece can go to diagonally + there code</returns>
	std::vector<struct positionAndCode> getAllDiagonalMoves(Board& board) const;

	/// <summary>
	/// function will return all the positions that the piece can go to (diagonally) 
	/// *CAN ONLY RETURN ONE PART AT A TIME (below-left/below-right/above-left/above-right)
	/// for example: if the piece is in [3][3]
	///  the diagonal of positions that are below the piece and before it (left side of the board) are: [2][2], [1][1], [0][0]
	///  the diagonal of positions that are below the piece and after it (right side of the board) are: [2][4], [1][5], [0][6]
	///  the diagonal of positions that are above the piece and before it (left side of the board) are: [4][2], [5][1], [6][0]
	///  the diagonal of positions that are above the piece and after it (right side of the board) are: [4][4], [5][5], [6][6], [7][7]
	/// </summary>
	/// <param name="returnPart">what part will the function return</param>
	/// <returns>
	/// if 0 : returns the diagonal of positions that are below the piece and before it (left side of the board)
	/// if 1 : returns the diagonal of positions that are below the piece and after it (right side of the board)
	/// if 2 : returns the diagonal of positions that are above the piece and before it (left side of the board)
	/// else : returns the diagonal of positions that are above the piece and after it (right side of the board)
	/// /returns>
	std::vector<Pos> getAllPosDiagonaly(int returnPart) const;

	/// <summary>
	/// this function is used by Rook and Queen
	/// </summary>
	/// <param name="board">the board itself</param>
	/// <returns>returns all the positions the piece can go as a cross + there code</returns>
	std::vector<struct positionAndCode> getAllCrossMoves(Board& board) const;

	/// <summary>
	/// function will return all the positions that are before/after the piece
	/// for example: if the piece is in [5][4]
	///  the positions before the piece are: [5][3], [5][2], [5][1], [5][0] 
	///  the positions after  the piece are: [5][5], [5][6], [5][7]
	/// </summary>
	/// <param name="beforePiece">what will the function return? the positions before or after the piece</param>
	/// <returns>if beforePiece is true then all the positions before the piece, else all the positions after it</returns>
	std::vector<Pos> getAllPosInRow(bool beforePiece) const;

	/// <summary>
	/// function will return all the positions that are below/above the piece
	/// for example: if the piece is in [5][4]
	///  the positions below the piece are: [4][4], [3][4], [2][4], [1][4], [0][4]
	///  the positions above the piece are: [6][4], [7][4]
	/// </summary>
	/// <param name="belowPiece">what will the function return? the positions above or below the piece</param>
	/// <returns>if belowPiece is true then all the positions below the piece, else all the positions above it</returns>
	std::vector<Pos> getAllPosInColumn(bool belowPiece) const;
};