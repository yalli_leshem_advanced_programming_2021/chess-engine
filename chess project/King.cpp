#include "King.h"

King::King(const bool white, const Pos pos) : Piece(white, pos, false)
{}

std::vector<struct positionAndCode> King::getAllMoves(Board& board) const
{
	int MIN_Y = (this->_pos.getY() == 0) ? this->_pos.getY() : this->_pos.getY() - 1;
	int MAX_Y = (this->_pos.getY() == 7) ? this->_pos.getY() : this->_pos.getY() + 1;
	int MIN_X = (this->_pos.getX() == 0) ? this->_pos.getX() : this->_pos.getX() - 1;
	int MAX_X = (this->_pos.getX() == 7) ? this->_pos.getX() : this->_pos.getX() + 1;

	std::vector<struct positionAndCode> allMoves;

	for (int y = MIN_Y; y <= MAX_Y; y++)
	{
		for (int x = MIN_X; x <= MAX_X; x++)
		{
			struct positionAndCode posNCode;
			posNCode.pos = Pos(x, y);
			posNCode.code = Codes::VALID;
			

			//	If there is a piece the same color as the King, we cant move it
			Piece* piece = board.getPiece(Pos(x, y));
			if (!piece->isEmpty() && piece->isWhite() == this->_white)
			{
				posNCode.code = Codes::SAME_COLOR;
			}
			else if (willBeChecked(posNCode.pos, board))
				posNCode.code = Codes::SELF_CHECK;	

			allMoves.push_back(posNCode);
		}
	}

	return allMoves;
}

bool King::isKing() const
{
	return true;
}

bool King::willBeChecked(Pos newPos, Board& board) const
{
	Codes moveCode = Codes::VALID;

	board.nextTurn();
	board.swapPieces(this->_pos, newPos);
	// check for self check
	for (int y = 0; y < 8; y++) { // go through all the positions in the board
		for (int x = 0; x < 8; x++)
		{
			Piece* piece = board.getPiece(Pos(x, y));
			if (!piece->isKing()) // to avoid infinite calling, check only if piece isnt a king
			{
				if (Codes::VALID == piece->isMoveValid(newPos, board)) // check if the move is possible if it is then self check happend
				{
					moveCode = Codes::SELF_CHECK;
				}
			}
		}
	}
	board.nextTurn();
	board.swapPieces(this->_pos, newPos);

	return (Codes::SELF_CHECK == moveCode);
}
