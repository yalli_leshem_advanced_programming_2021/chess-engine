#include "Rook.h"

Rook::Rook(const bool white, const Pos pos) : Piece(white, pos, false)
{}

std::vector<struct positionAndCode> Rook::getAllMoves(Board& board) const
{
	return getAllCrossMoves(board);
}

std::vector<struct positionAndCode> Rook::checkOnePartOfPositions(std::vector<Pos> partOfAllPos, Board& board) const
{
	bool isBlocked = false;
	std::vector<struct positionAndCode> allMoves;

	for (Pos pos : partOfAllPos)
	{
		// make new position type
		struct positionAndCode posNCode;
		posNCode.pos = pos;
		posNCode.code = Codes::VALID;

		if (!isBlocked) // if position is already blocked, no need to check the code, it's invalid automatically
		{
			//	If there is a piece, and it is the same color as this
			Piece* piece = board.getPiece(pos);
			if (!piece->isEmpty()) // if theres a piece in this pos
			{
				if (piece->isWhite() == this->_white) {
					posNCode.code = Codes::SAME_COLOR;
					isBlocked = true;
				}
				else // if theres a piece but its a different color, code is valid and the rest of the positions are blocked
				{
					// TODO: check for check/mate add code to check/checkmate accordingly
					posNCode.code = Codes::VALID;
					isBlocked = true;
				}
			}
		}
		else // if blocked then the move is illegal
			posNCode.code = Codes::ILLEGAL_MOVE;

		allMoves.push_back(posNCode);
	}

	return allMoves;
}

std::vector<struct positionAndCode> Rook::getAllCrossMoves(Board& board) const
{
	enum { BEFORE, AFTER, BELOW, ABOVE };

	std::vector<struct positionAndCode> allMoves;

	bool isBlocked = false;

	// first we get all the moves the piece can make but we devide them into 4 parts (below, above, before and after the piece)
	std::vector< std::vector<Pos> > allPos;
	allPos.push_back(this->getAllPosInRow(true));
	allPos.push_back(this->getAllPosInRow(false));
	allPos.push_back(this->getAllPosInColumn(true));
	allPos.push_back(this->getAllPosInColumn(false));

	// second thing we do we reverse the order of the below and before positions (so we can use a simpler code)
	std::reverse(allPos[BEFORE].begin(), allPos[BEFORE].end());
	std::reverse(allPos[BELOW].begin(), allPos[BELOW].end());

	// now, in each part we'll only keep the valid moves
	for (std::vector<Pos> partOfAllPos : allPos)
	{
		std::vector<struct positionAndCode> parOfAllMoves = checkOnePartOfPositions(partOfAllPos, board);

		// push all the positions to the vector
		for (struct positionAndCode posNcode : parOfAllMoves)
			allMoves.push_back(posNcode);
	}

	return allMoves;
}

std::vector<Pos> Rook::getAllPosInRow(const bool beforePiece) const
{
	const int MIN_X = 0;
	const int MAX_X = 8;

	int y = this->_pos.getY();
	std::vector<Pos> posAfterPiece;
	std::vector<Pos> posBeforePiece;

	// add all indexes from [y][0] to [y][7]
	for (int x = MIN_X; x < MAX_X; x++)
	{
		if (x < this->_pos.getX()) // while x is lower then the x of the piece, enter it to the vector of positions before the piece
			posBeforePiece.push_back(Pos(x, y));
		else if (x > this->_pos.getX()) // adds all the positions with x that is bigger then the x of the piece to the vector of positions after the piece
			posAfterPiece.push_back(Pos(x, y));
	}

	if (beforePiece) // if true, return all positions before the piece
		return posBeforePiece;

	return posAfterPiece; // if not then return all the positions after the piece

}

std::vector<Pos> Rook::getAllPosInColumn(bool belowPiece) const
{
	const int MIN_Y = 0;
	const int MAX_Y = 8;

	int x = this->_pos.getX();
	std::vector<Pos> posAbovePiece;
	std::vector<Pos> posBelowPiece;

	// add all indexes from [0][x] to [7][x]
	for (int y = MIN_Y; y < MAX_Y; y++)
	{
		if (y < this->_pos.getY()) // while y is lower then the y of the piece, enter it to the vector of positions below the piece
			posBelowPiece.push_back(Pos(x, y));
		else if (y > this->_pos.getY()) // adds all the positions with y that is bigger then the y of the piece to the vector of positions above the piece
			posAbovePiece.push_back(Pos(x, y));
	}

	if (belowPiece) // if true, return all positions below the piece
		return posBelowPiece;

	return posAbovePiece; // if not then return all the positions above the piece
}