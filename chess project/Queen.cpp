#include "Queen.h"

Queen::Queen(const bool white, const Pos pos) : Piece(white, pos, false)
{}

std::vector<struct positionAndCode> Queen::getAllMoves(Board& board) const
{
	std::vector<struct positionAndCode> allDiagonalMoves = getAllDiagonalMoves(board);
	std::vector<struct positionAndCode> allMoves = getAllCrossMoves(board);

	for (struct positionAndCode posNCode : allDiagonalMoves)
		allMoves.push_back(posNCode);

	return allMoves;
}

std::vector<struct positionAndCode> Queen::checkOnePartOfPositions(std::vector<Pos> partOfAllPos, Board& board) const
{
	bool isBlocked = false;
	std::vector<struct positionAndCode> allMoves;

	for (Pos pos : partOfAllPos)
	{
		// make new position type
		struct positionAndCode posNCode;
		posNCode.pos = pos;
		posNCode.code = Codes::VALID;

		if (!isBlocked) // if position is already blocked, no need to check the code, it's invalid automatically
		{
			//	If there is a piece, and it is the same color as this
			Piece* piece = board.getPiece(pos);
			if (!piece->isEmpty()) // if theres a piece in this pos
			{
				if (piece->isWhite() == this->_white) {
					posNCode.code = Codes::SAME_COLOR;
					isBlocked = true;
				}
				else // if theres a piece but its a different color, code is valid and the rest of the positions are blocked
				{
					// TODO: check for check/mate add code to check/checkmate accordingly
					posNCode.code = Codes::VALID;
					isBlocked = true;
				}
			}
		}
		else // if blocked then the move is illegal
			posNCode.code = Codes::ILLEGAL_MOVE;

		allMoves.push_back(posNCode);
	}

	return allMoves;
}

std::vector<struct positionAndCode> Queen::getAllDiagonalMoves(Board& board) const
{
	enum { BELOW_LEFT, BELOW_RIGHT, ABOVE_LEFT, ABOVE_RIGHT };
	std::vector<struct positionAndCode> allMoves;

	// go through all diagonals
	for (int i = BELOW_LEFT; i <= ABOVE_RIGHT; i++)
	{
		// get each part of the positions 
		std::vector<Pos> oneDiagonal = getAllPosDiagonaly(i);

		// now, if the vector is of the positions below the piece, we'll need to reverse them so we'll be able to correctly check them
		if (i == BELOW_LEFT || i == BELOW_RIGHT)
			std::reverse(oneDiagonal.begin(), oneDiagonal.end());

		// get code for each part
		std::vector<struct positionAndCode> parOfAllMoves = checkOnePartOfPositions(oneDiagonal, board);

		// push all the positions to the vector
		for (struct positionAndCode posNcode : parOfAllMoves)
			allMoves.push_back(posNcode);
	}

	return allMoves;
}

std::vector<Pos> Queen::getAllPosDiagonaly(int returnPart) const
{
	enum { BELOW_LEFT, BELOW_RIGHT, ABOVE_LEFT, ABOVE_RIGHT };

	const int MIN_X = 0;
	const int MAX_X = 8;

	const int MIN_Y = 0;
	const int MAX_Y = 8;

	int currY = this->_pos.getY();
	int currX = this->_pos.getX();

	std::vector<Pos> posBelowLPiece;
	std::vector<Pos> posBelowRPiece;
	std::vector<Pos> posAboveLPiece;
	std::vector<Pos> posAboveRPiece;

	for (int y = MIN_Y; y < MAX_Y; y++)
	{
		int yDifference = currY - y; // to get the next x in the diagonal we simply get the difference between the currenty and the new y and add/sub it from x	

		int xM = currX - yDifference; // xM(minus) 
		int xP = currX + yDifference; // xP(plus)

		if (xM >= MIN_X && xM < MAX_X) // only if x >= 0 and x < 8 the position is valid, and we can make a new position 
		{
			Pos posM = Pos(xM, y); // posM(minus)

			if (y > currY) // if we are checking the upper diagonal: -- == + and because of that we add this position to the posAboveR vector
				posAboveRPiece.push_back(posM);
			else if (currY > y) // if we are checking the lower diagonal: -+ == - and because of that we add this position to the posBelowL vector
				posBelowLPiece.push_back(posM);
		}

		if (xP >= MIN_X && xP < MAX_X) // only if x >= 0 and x < 8 the position is valid, and we can make a new position 
		{
			Pos posP = Pos(xP, y); // posP(plus)

			if (y > currY) // if we are checking the upper diagonal: -+ == - and because of that we add this position to the posAboveL vector
				posAboveLPiece.push_back(posP);
			else if (currY > y) // if we are checking the lower diagonal: ++ == + and because of that we add this position to the posBelowR vector
				posBelowRPiece.push_back(posP);
		}
	}

	if (BELOW_LEFT == returnPart) // if returnPart is 0 : return the posBelowL vector
		return posBelowLPiece;
	else if (BELOW_RIGHT == returnPart) // if returnPart is 1 : return the posBelowR vector
		return posBelowRPiece;
	else if (ABOVE_LEFT == returnPart) // if returnPart is 2 : return the posAboveL vector
		return posAboveLPiece;
	else // if non of the above was entered : return the posAboveR vector
		return posAboveRPiece;
}

std::vector<struct positionAndCode> Queen::getAllCrossMoves(Board& board) const
{
	enum { BEFORE, AFTER, BELOW, ABOVE };

	std::vector<struct positionAndCode> allMoves;

	bool isBlocked = false;

	// first we get all the moves the piece can make but we devide them into 4 parts (below, above, before and after the piece)
	std::vector< std::vector<Pos> > allPos;
	allPos.push_back(this->getAllPosInRow(true));
	allPos.push_back(this->getAllPosInRow(false));
	allPos.push_back(this->getAllPosInColumn(true));
	allPos.push_back(this->getAllPosInColumn(false));

	// second thing we do we reverse the order of the below and before positions (so we can use a simpler code)
	std::reverse(allPos[BEFORE].begin(), allPos[BEFORE].end());
	std::reverse(allPos[BELOW].begin(), allPos[BELOW].end());

	// now, in each part we'll only keep the valid moves
	for (std::vector<Pos> partOfAllPos : allPos)
	{
		std::vector<struct positionAndCode> parOfAllMoves = checkOnePartOfPositions(partOfAllPos, board);

		// push all the positions to the vector
		for (struct positionAndCode posNcode : parOfAllMoves)
			allMoves.push_back(posNcode);
	}

	return allMoves;
}

std::vector<Pos> Queen::getAllPosInRow(const bool beforePiece) const
{
	const int MIN_X = 0;
	const int MAX_X = 8;

	int y = this->_pos.getY();
	std::vector<Pos> posAfterPiece;
	std::vector<Pos> posBeforePiece;

	// add all indexes from [y][0] to [y][7]
	for (int x = MIN_X; x < MAX_X; x++)
	{
		if (x < this->_pos.getX()) // while x is lower then the x of the piece, enter it to the vector of positions before the piece
			posBeforePiece.push_back(Pos(x, y));
		if (x > this->_pos.getX()) // adds all the positions with x that is bigger then the x of the piece to the vector of positions after the piece
			posAfterPiece.push_back(Pos(x, y));
	}

	if (beforePiece) // if true, return all positions before the piece
		return posBeforePiece;

	return posAfterPiece; // if not then return all the positions after the piece

}

std::vector<Pos> Queen::getAllPosInColumn(bool belowPiece) const
{
	const int MIN_Y = 0;
	const int MAX_Y = 8;

	int x = this->_pos.getX();
	std::vector<Pos> posAbovePiece;
	std::vector<Pos> posBelowPiece;

	// add all indexes from [0][x] to [7][x]
	for (int y = MIN_Y; y < MAX_Y; y++)
	{
		if (y < this->_pos.getY()) // while y is lower then the y of the piece, enter it to the vector of positions below the piece
			posBelowPiece.push_back(Pos(x, y));
		else if (y > this->_pos.getY()) // adds all the positions with y that is bigger then the y of the piece to the vector of positions above the piece
			posAbovePiece.push_back(Pos(x, y));
	}

	if (belowPiece) // if true, return all positions below the piece
		return posBelowPiece;

	return posAbovePiece; // if not then return all the positions above the piece
}