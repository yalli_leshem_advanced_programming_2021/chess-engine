/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "EmptyPiece.h"
#include "Queen.h"
#include "Rook.h"
#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;

void initBoard(Board& board);

void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	
	Board board = Board(); // make new board object
	initBoard(board);

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"); // 
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();


	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		// YOUR CODE
		char code = board.playTurn(msgFromGraphics);
		char sendCode[2];
		sendCode[0] = code;
		sendCode[1] = 0;
		strcpy_s(msgToGraphics, sendCode); // msgToGraphics should contain the result of the operation

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}

/// <summary>
/// function initializes the board
/// </summary>
void initBoard(Board& board) {

	const char bRook = 'r';
	const char wRook = 'R';
	const char bKnight = 'n';
	const char wKnight = 'N';
	const char bBishop = 'b';
	const char wBishop = 'B';
	const char bQueen = 'q';
	const char wQueen = 'Q';
	const char bKing = 'k';
	const char wKing = 'K';
	const char bPawn = 'p';
	const char wPawn = 'P';
	const char empty = '#';

	const int MIN_XY = 0;
	const int MAX_XY = 7;
	int y = 0;
	int x = 0;

	std::string formatedBoard = "RNBQKBNRPPPPPPPP################################pppppppprnbqkbnr";

	for (char p : formatedBoard)
	{
		if (x > MAX_XY) // if went through all the columns reset to 0 and inc y
		{
			x = MIN_XY;
			y++;
		}
		Pos pos = Pos(x, y);

		switch (p)
		{
		case bKing:
			board.insertPiece(new King(false, pos));
			break;
		case wKing:
			board.insertPiece(new King(true, pos));
			break;
		case bBishop:
			board.insertPiece(new Bishop(false, pos));
			break;
		case wBishop:
			board.insertPiece(new Bishop(true, pos));
			break;
		case bKnight:
			board.insertPiece(new Knight(false, pos));
			break;
		case wKnight:
			board.insertPiece(new Knight(true, pos));
			break;
		case bPawn:
			board.insertPiece(new Pawn(false, pos));
			break;
		case wPawn:
			board.insertPiece(new Pawn(true, pos));
			break;
		case bQueen:
			board.insertPiece(new Queen(false, pos));
			break;
		case wQueen:
			board.insertPiece(new Queen(true, pos));
			break;
		case bRook:
			board.insertPiece(new Rook(false, pos));
			break;
		case wRook:
			board.insertPiece(new Rook(true, pos));
			break;
		case empty:
			board.insertPiece(new EmptyPiece(pos));
			break;
		default:
			std::cout << "icaramba!" << std::endl;
			break;
		}

		x++;
	}
}