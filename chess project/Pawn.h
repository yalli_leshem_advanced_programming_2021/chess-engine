#pragma once

#include "Piece.h"
#include "Pos.h"
#include "Board.h"
class Board;

class Pawn : public Piece {
public:
	/// <summary>
	///	Create a pawn piece
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	Pawn(const bool white, const Pos pos);

	std::vector<struct positionAndCode> getAllMoves(Board& board) const override;

private:

	/// <summary>
	/// function checks if the pawn can go up/down and if it can go 2 up/down
	/// </summary>
	/// <returns>returns the two positions the pawn can go to (or one)</returns>
	std::vector<Pos> checkIfCanMoveUp() const;
	
	/// <summary>
	/// function returns the two positions the pawn can eat in
	/// </summary>
	/// <returns>returns the two positions</returns>
	std::vector<Pos> checkIfCanEat() const;
};