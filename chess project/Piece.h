#pragma once

#include <vector>
#include "Pos.h"
#include "Board.h"

/// <summary>
/// this enum contains all the possible codes that can be returned
/// codes:
///		| 0 - valid move                                                												  
///		| 1 - valid move, check                                              
///		| 2 - the src position doesn't have a piece of the current user    
///		| 3 - invalid move, destination has a piece of the current user
///		| 4 - invalid move, will make self check						
///		| 5 - positions are illegal									
///		| 6 - illegal move												 
///		| 7 - invalid move, dest pos and src pos are the same			 
///		| 8 - valid move, checkmate |										
/// </summary>
enum class Codes { VALID, CHECK, NO_PIECE, SAME_COLOR, SELF_CHECK, ILLEGAL_POS, ILLEGAL_MOVE, SAME_DST_AND_SRC_POS, CHECKMATE };

/// <summary>
/// possible codes are: 0, 1, 3, 4, 6, 8 (explanation in enum codes)
/// </summary>
struct positionAndCode
{
	Pos pos;
	Codes code;
};

class Board;

/// <summary>
/// master class of all the pieces (subclasses will be: | Queen | King | Rook | Bishop | Knight | Pawn |)
/// </summary>
class Piece {
protected:
	bool _white;
	Pos _pos;
	bool _empty;
	static Pos _kingsPos[2];

public:

	/// <summary>
	///	constructor: adds to the board the new piece
	/// 
	/// Does not assign the piece to a board. make sure to call insertPiece before using this object.
	/// </summary>
	/// <param name="white">is the piece white</param>
	/// <param name="pos">the position of the piece on the board</param>
	/// <param name="empty">is this a piece or an empty position (by default true)</param>
	Piece(const bool white, const Pos pos, const bool empty = true);

	Pos getPos() const;
	bool isWhite() const;
	bool isEmpty() const;
	void setPos(const Pos newPos);

	/// <summary>
	/// Returns a vector of all the positions the piece can go to and the code to all the moves (according to the notes in struct 'positonAndCode')
	/// each piece moves differently so this is virtual
	/// </summary>
	/// <param name="board">the board itself</param>
	/// <returns>a vector of all the moves the piece can make and there code (only checks codes: 0, 3, 4, 6)</returns>
	virtual std::vector<struct positionAndCode> getAllMoves(Board& board) const = 0;

	virtual ~Piece() = default;

	/// <summary>
	/// checks if the move is valid
	/// returns the code of the move
	/// covers all the codes (more info at the codes at the enum 'code' declaration) except for codes: 1, 2 and 5
	/// </summary>
	/// <param name="newPos">the targeted position</param>
	/// <param name="board">the board itself</param>
	/// <returns>the code of the move</returns>
	Codes isMoveValid(const Pos newPos, Board& board) const;

	/// <summary>
	/// Moves the piece to another position (and creates an empty piece in the old position)
	/// will only work if move is valid
	/// </summary>
	/// <param name="newPos">the targeted position</param>
	/// <param name="board">the board itself</param>
	/// <returns>the code of the move</returns>
	Codes movePiece(const Pos newPos, Board& board);

	/// <summary>
	/// only king object will override this function
	/// </summary>
	/// <returns>true if the object is a king</returns>
	virtual bool isKing() const;

	/// <summary>
	/// covers all the codes possible (calls isMoveValid, then checks for codes 1 and 2)
	/// </summary>
	/// <param name="newPos">the targeted position</param>
	/// <param name="board">the board itself</param>
	/// <returns>the code of the move</returns>
	Codes coverAllCodes(Pos newPos, Board& board);

	/// <summary>
	/// function checks for check mate
	/// </summary>
	/// <param name="board">the board itself</param>
	/// <returns>true if checkMate was done</returns>
	bool checkCheckMate(Board& board) const;
};