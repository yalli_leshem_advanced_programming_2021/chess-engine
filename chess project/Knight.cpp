#include "Knight.h"

Knight::Knight(const bool white, const Pos pos) : Piece(white, pos, false)
{}

std::vector<struct positionAndCode> Knight::getAllMoves(Board& board) const
{
	std::vector<struct positionAndCode> allMoves;
	std::vector<Pos> allPositions = getAllPos();

	for (Pos pos : allPositions)
	{
		// make new position type
		struct positionAndCode posNCode;
		posNCode.pos = pos;

		//	If there is a piece the same color as the Knight, we cant move it
		Piece* piece = board.getPiece(pos);
		if (!piece->isEmpty() && piece->isWhite() == this->_white)
		{
			posNCode.code = Codes::SAME_COLOR;
		}
		else // knight cannot be blocked
		{
			posNCode.code = Codes::VALID;
		}

		allMoves.push_back(posNCode);
	}

	return allMoves;
}

std::vector<Pos> Knight::getAllPos() const
{
	const int MIN_X = 0;
	const int MAX_X = 8;
	const int MIN_Y = 0;
	const int MAX_Y = 8;
	const int MOVING_RANGE = 2;

	std::vector<Pos> allPos;

	int currY = this->_pos.getY();
	int currX = this->_pos.getX();

	// go through all the positions
	for (int y = currY - MOVING_RANGE; y <= currY + MOVING_RANGE; y++)
	{
		if (y < MAX_Y && MIN_Y < y) // check that we arent out of range
		{
			// if the y is 2 down/up from the current y, the x will be 1 back/forward 
			if (y == currY - MOVING_RANGE || y == currY + MOVING_RANGE)
			{
				if (currX + 1 < MAX_X) // make sure we don't go beyond the board limits
					allPos.push_back(Pos(currX + 1, y));
				if (MIN_X <= currX - 1) // make sure we don't go beyond the board limits
					allPos.push_back(Pos(currX - 1, y));
			}
			else if (y == currY - 1 || y == currY + 1) // if the y is 1 up/down from the current y, the x will be 2 back/forward
			{
				if (currX + 2 < MAX_X && y) // make sure we don't go beyond the board limits
					allPos.push_back(Pos(currX + 2, y));
				if (MIN_X <= currX - 2) // make sure we don't go beyond the board limits
					allPos.push_back(Pos(currX - 2, y));
			}
		}
	}

	return allPos;
}
