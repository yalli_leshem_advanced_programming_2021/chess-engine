#include "Piece.h"

Pos Piece::_kingsPos[2] = { Pos("e8"), Pos("e2") };

Piece::Piece(const bool white, const Pos pos, const bool empty) : _white(white), _pos(pos), _empty(empty) {}

Pos Piece::getPos() const
{
	return this->_pos;
}

bool Piece::isWhite() const
{
	return this->_white;
}

bool Piece::isEmpty() const
{
	return this->_empty;
}

void Piece::setPos(const Pos newPos)
{
	this->_pos = newPos;
}

Codes Piece::isMoveValid(const Pos newPos, Board& board) const
{
	Codes moveCode = Codes::ILLEGAL_MOVE; // default code

	if (board.isWhitesTurn() != this->_white || this->_empty) // checks for code 2
		moveCode = Codes::NO_PIECE;
	else if (this->_pos == newPos) // checks for code 7
		moveCode = Codes::SAME_DST_AND_SRC_POS;
	else // checks for codes: 0, 3, 4, 6 and 8
	{
		std::vector<struct positionAndCode> AllMoves = this->getAllMoves(board);

		for (struct positionAndCode validPos : AllMoves)
		{
			if (validPos.pos == newPos)
				moveCode = validPos.code;
		}
	}

	return moveCode;
}

Codes Piece::movePiece(const Pos newPos, Board& board)
{
	Codes moveCode = coverAllCodes(newPos, board);

	// only if the code is one of three that indicates a valid move
	if (Codes::VALID == moveCode || Codes::CHECK == moveCode || Codes::CHECKMATE == moveCode)
	{

		// get the piece in the new position
		Piece* otherP = board.getPiece(newPos);

		// swap the positions (the var not the actual position on the board)
		std::swap(otherP->_pos, this->_pos);

		// empty the other piece before actually swapping them
		otherP->_empty = true;
		board.swapPieces(otherP->getPos(), this->getPos());

		if (this->isKing())
		{
			int kingColor = (this->_white) ? 1 : 0;
			_kingsPos[kingColor] = this->_pos;
		}
		// only if moved go to the next turn
		board.nextTurn();
	}
	if (moveCode == Codes::CHECK) // if check, check for check mate
	{

		if (this->checkCheckMate(board)) // if returns true, checkMate
			moveCode = Codes::CHECKMATE;
	}

	return moveCode;
}

bool Piece::isKing() const
{
	return false;
}

Codes Piece::coverAllCodes(Pos newPos, Board& board)
{
	const int WHITE_KING = 1;
	const int BLACK_KING = 0;

	Codes moveCode = isMoveValid(newPos, board);
	int kingColor = (this->_white) ? WHITE_KING: BLACK_KING;
	int rivalKingColor = (this->_white) ? BLACK_KING : WHITE_KING;

	if (moveCode == Codes::VALID && !this->isKing()) // only if the move is valid we can check for codes 1 and 2
	{
		// make the piece in the new position empty 
		Piece* p = board.getPiece(newPos);
		bool wasEmpty = p->_empty;
		p->_empty = true;

		// swap between the current piece and the one in the other position to check if made self check/check
		board.swapPieces(this->_pos, newPos);
		std::swap(this->_pos, newPos);
		board.nextTurn();
		// check for self check
		for (int y = 0; y < 8; y++) { // go through all the positions in the board
			for (int x = 0; x < 8; x++)
			{
				Piece* piece = board.getPiece(Pos(x, y));
				if (!piece->isKing()) // to avoid infinite calling, check only if piece isnt a king
				{
					if (Codes::VALID == piece->isMoveValid(_kingsPos[kingColor], board)) // check if the move is possible if it is then self check happend
					{
						// return the board back to normal
						board.nextTurn();
						p->_empty = wasEmpty;
						std::swap(this->_pos, newPos);
						board.swapPieces(newPos, this->_pos);

						return Codes::SELF_CHECK;
					}
				}
			}
		}

		board.nextTurn();
		
		if (Codes::VALID == this->isMoveValid(_kingsPos[rivalKingColor], board)) // check for check
			moveCode = Codes::CHECK;

		// return the board back to normal
		std::swap(this->_pos, newPos);
		p->_empty = wasEmpty;
		board.swapPieces(newPos, this->_pos);
	}

	return moveCode;
}

bool Piece::checkCheckMate(Board& board) const
{
	Pos oldPos = this->getPos(); // to check nothing was messed up

	for (int y = 0; y < 8; y++) { // go through all the pieces in the board
		for (int x = 0; x < 8; x++)
		{
			Piece* piece = board.getPiece(Pos(x, y));
			Pos pOldPos = piece->getPos();
			if (!piece->isKing())
			{
				for (int yPos = 0; yPos < 8; yPos++) { // go through all the positions on the board 
					for (int xPos = 0; xPos < 8; xPos++)
					{
						Codes tryMoving = piece->coverAllCodes(Pos(yPos, xPos), board); // for each move on the pece try to move the piece

						// make kind of checksum by making sure non of the values are changed during the check
						if (tryMoving == Codes::VALID && pOldPos == piece->getPos() && oldPos == this->getPos()) // if any piece can move at all, its not checkMate
						{
							piece->_pos = pOldPos;
							return false;
						}
					}
				}
			}
			piece->_pos = pOldPos;		
		}
	} 
	
	return true; // if we went through all the pieces and non of them could move, its checkMate
}
