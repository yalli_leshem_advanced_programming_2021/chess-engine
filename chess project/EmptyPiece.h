#pragma once

#include "Piece.h"
#include "Pos.h"
#include "Board.h"
class Board;

class EmptyPiece : public Piece {
public:
	/// <summary>
	///	Create an empty piece
	/// </summary>
	/// <param name="pos">the position of the piece on the board</param>
	EmptyPiece(const Pos pos);

	std::vector<struct positionAndCode> getAllMoves(Board& board) const override;
};